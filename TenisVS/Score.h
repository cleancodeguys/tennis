#pragma once
#include <cstdint>

enum class Score:std::uint8_t
{
	Zero, 
	One, 
	Two,
	Three
};