#ifndef CPP_TENNIS_H
#define CPP_TENNIS_H

#include <string>
#include "Score.h"

const std::string tennisScore(int p1Score, int p2Score);

#endif //CPP_TENNIS_H
