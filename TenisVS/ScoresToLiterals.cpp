#include "ScoresToLiterals.h"

const std::unordered_map<Score, std::string> ScoresToLiterals::equalScoreToString =  { {Score::Zero,"Love-All"},{Score::One, "Fifteen-All"},
		{Score::Two ,"Thirty-All"} };

const std::unordered_map<Score, std::string>  ScoresToLiterals::displayedScoreToString = { {Score::Zero,"Love"},{Score::One, "Fifteen"},
		{Score::Two ,"Thirty"},{Score::Three,"Forty" } };