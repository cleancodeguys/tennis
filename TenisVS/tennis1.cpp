#include "tennis.h"
#include "ScoresToLiterals.h"
#include "OperationType.h"

bool assignScoreString(std::string & score, Score playerScore, const std::unordered_map<Score, std::string>& map, OperationType operationType)
{
	auto it = map.find(playerScore);
	if (it != std::end(map))
	{
		switch (operationType)
		{
		case OperationType::Concatenate:
			score += it->second;
			break;
		case OperationType::Assignment:
			score = it->second;
			break;
		}

		return true;
	}
	return false;
}


void equalScoreCase(Score playerScore, std::string & score)
{
	if(!assignScoreString(score, playerScore, ScoresToLiterals::equalScoreToString, OperationType::Assignment))
	{
		score = "Deuce";
	}
}




void advantageScoreCase(Score firstPlayerScore, Score secondPlayerScore, std::string& score)
{
	int difference = static_cast<int>(firstPlayerScore) - static_cast<int>(secondPlayerScore);

	//const int kFirstPlayerInAdvantage = 1;
	if (difference == 1)
	{
		score = "Advantage player1";
	}
	else if (difference == -1)
	{
		score = "Advantage player2";
	}

	else if (difference >= 2)
	{
		score = "Win for player1";
	}
	else
	{
		score = "Win for player2";
	}
}

void thirdCase(Score firstPlayerScore, Score secondPlayerScore, std::string& score, Score & temporaryScore)
{
	const int kNumberOfPlayers = 2;
	const int kFirstPlayerIndex = 0;
	for (int index = 0; index < kNumberOfPlayers; index++)
	{
		if (index == kFirstPlayerIndex)
		{
			temporaryScore = firstPlayerScore;
		}
		else
		{
			score += "-";
			temporaryScore = secondPlayerScore;
		}

		assignScoreString(score, temporaryScore, ScoresToLiterals::displayedScoreToString, OperationType::Concatenate);
	}
}


const std::string tennisScore(int firstPlayerScore, int secondPlayerScore) {
	std::string score = "";
	Score temporaryScore = Score::Zero;
	const int kPlayerAdvantage = 4;
	if (firstPlayerScore == secondPlayerScore)
	{
		equalScoreCase(static_cast<Score>(firstPlayerScore), score);
	}
	else if (static_cast<int>(firstPlayerScore) >= kPlayerAdvantage || static_cast<int>(secondPlayerScore) >= kPlayerAdvantage)
	{
		advantageScoreCase(static_cast<Score>(firstPlayerScore), static_cast<Score>(secondPlayerScore), score);
	}
	else
	{
		thirdCase(static_cast<Score>(firstPlayerScore), static_cast<Score>(secondPlayerScore), score, temporaryScore);
	}
	return score;
}
