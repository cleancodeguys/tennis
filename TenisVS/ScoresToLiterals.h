#pragma once
#include <unordered_map>
#include "Score.h"
#include <string>

class ScoresToLiterals
{
public:
	static const std::unordered_map<Score, std::string>  equalScoreToString;
	static const std::unordered_map<Score, std::string>  displayedScoreToString;

};